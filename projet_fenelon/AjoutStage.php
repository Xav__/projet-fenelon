<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title id="titre">Fenelon - Gestion Stage</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    
</head>

    <body>

        <?php
            include("navbar.php");

            $bdd = new PDO('mysql:host=localhost;dbname=projet_fenelon', 'root', '');
        ?>

       
       <!-- Formulaire ajout stage -->
       <div class="container">

       <form method="post" action="ajout_stage_post.php">
            <label><b>Formulaire d'ajout d'un nouveau stage dans la base de données.</b></label>
            <br>
            <b> *  Les champs sont obligatoires. </b>

            <h1> Formulaire - Ajout Stage</h1>

    
                <div class="row">

                <div class="col-sm">

                    <h1>Élève *</h1>

                    

                    <select name = 'id_eleve'>

                    <?php

                            $reponse = $bdd->query('SELECT * FROM eleves ORDER BY nom_eleve');

                            while ($donnees = $reponse->fetch()){
                            
                                echo("<option value='".  $donnees['id_eleve']."'> ". $donnees['nom_eleve']. " " .$donnees['prenom_eleve'] . "</option>");

                            }

                        ?>

                    </select>


                </div>

                <div class="col-sm">

                    <h1>Entreprise *</h1>

                    <select name = 'id_entreprise'>

                    <?php

                        $reponse = $bdd->query('SELECT * FROM entreprises ORDER BY Nom_entreprise');

                        while ($donnees = $reponse->fetch()){
                            
                            echo("<option value='".  $donnees['id_entreprise']."'> ". $donnees['Nom_entreprise'] . " - " . $donnees['Adresse1'] . "</option>");

                        }
                    
                    ?>
                        
                    </select>

                    
                </div>

                <div class="col-sm">

                    <br>
                    <label><b>Classe pendant le stage *</b></label>
                    <input type="text" placeholder="Entrer la classe actuelle du stagiaire" name="classe_pdt_stage" required>
                    <br>
                    <br>
                    <?php
                        if(isset($_GET['Date']) && $_GET['Date'] == 'false'){
                            echo("<h4 id='Info_message'> La date de fin de stage ne peut pas être avant celle du début. </h4>");
                        }
                    ?>
                    <label><b>Date de début du stage *</b></label>
                    <input type="date" placeholder="Entrer la date de debut du stage" name="date_debut" required>
                    <br>
                    <label><b>Date de fin du stage *</b></label>
                    <input type="date" placeholder="Entrer la date de fin du stage" name="date_fin" required>
                    <br>
                    <br>
                    <label><b>Nom tuteur *</b></label>
                    <input type="text" placeholder="Entrer le nom du tuteur de stage" name="nom_tuteur" required>

                    <label><b>Prénom tuteur</b></label>
                    <input type="text" placeholder="Entrer le prénom du tuteur de stage" name="prenom_tuteur">

                    <label><b>Fonction tuteur *</b></label>
                    <input type="text" placeholder="Entrer la fonction du tuteur de stage" name="fonction_tuteur" required>
                    
                    <label><b>Tel tuteur</b></label>
                    <input type="text" placeholder="Entrer le téléphone du tuteur de stage" name="tel_tuteur">

                    <label><b>Portable tuteur</b></label>
                    <input type="text" placeholder="Entrer le portable du tuteur de stage" name="portable_tuteur">

                    <label for="exampleInputEmail1"> Mail tuteur *</label>
                    <input type="email" class="form-control" name="mail_tuteur" aria-describedby="emailHelp" placeholder="Entrer le mail du tuteur de stage" required>
                    <br>
                    <label><b>Nom prof *</b></label>
                    <input type="text" placeholder="Entrer le nom du prof" name="nom_prof" required>

                    <label><b>Prénom prof</b></label>
                    <input type="text" placeholder="Entrer le prénom du prof" name="prenom_prof">

                </div>
        
                    <input type="submit" id='submit' value='Enregistrer' >

                </form>
            </div>
        </div>

        <!-- Différents affichages des infos messages selon les entrées de l'utilisateu -->
        <div class="container">
            <div class="col align-self-center">
                    
                <?php

                    if(isset($_GET['Va']) && $_GET['Va'] == 'true'){

                        echo("<h2 id='Info_message'> Le stage a bien été ajouté. </h2>");

                    }

                    if(isset($_GET['Va']) && $_GET['Va'] == 'false'){

                        echo("<h2 id='Info_message'> Le stage n'a pas été ajouté. </h2>");

                    }

                    if(isset($_GET['Del']) && $_GET['Del'] == 'true'){

                        echo("<h2 id='Info_message'> Le stage a bien été supprimé. </h2>");

                    }

                ?>
            </div>
        </div>

            <br><br>

        <!-- Suppresion de stage -->
        <div class="container">
            <div class="col align-self-center">
                <form method="post" action="ajout_stage_post.php?etape=1">
                    <label><b>Formulaire de suppression d'une stage dans la base de données.</b></label>
                    <br>
                    <label><b>Sélectionner l'élève du stage à supprimer :</b></label>
                    <br>
                    <select name ='id_eleve'>

                        <?php

                                $info_eleves = $bdd->query('SELECT * FROM eleves ORDER BY nom_eleve');

                                while ($donnees = $info_eleves->fetch()){
                                
                                    echo("<option value='".  $donnees['id_eleve']."'> ". $donnees['nom_eleve']. " - " .$donnees['prenom_eleve'] . "</option>");

                                }

                        ?>

                        <input type="submit" id='submit' value='Sélectionner' >

                    </select>
                </form>

                <br><br>

                <?php

                    // Affichage du tableau après la sélection de l'élève pour la suppresion de son stage
                    if(isset($_GET['E'])){
                        $req_eleve = $bdd->query("SELECT * FROM stages WHERE id_eleve ='". $_GET['E'] ."'");

                        $req_nom_prenom_eleves = $bdd->query("SELECT * FROM eleves WHERE id_eleve ='". $_GET['E'] ."'");
                        $nom_prenom_eleves = $req_nom_prenom_eleves->fetch();

                        echo ("Recherche pour l'élève : " . $nom_prenom_eleves['nom_eleve'] . " ". $nom_prenom_eleves['prenom_eleve'] . "<br>");
                        
                        echo("<table class='table table-bordered'>
                                <thead class='thead-light'>
                                    <tr>
                                        <th>
                                            Nom de l'entreprise
                                        </th>
                                        <th>
                                            Nom de l'élève
                                        </th>
                                        <th>
                                            Prénom de l'élève
                                        </th>
                                        <th>
                                            Date début stage
                                        </th>
                                        <th>
                                            Date fin stage
                                        </th>
                                        <th>
                                            Suprresion stage
                                        </th>
                                    </tr>
                                </thead>
                            <tbody>");

                        while ($donnees = $req_eleve->fetch()){

                            $req_nom_entreprise = $bdd->query("SELECT Nom_entreprise FROM entreprises WHERE id_entreprise ='". $donnees['id_entreprise'] ."'");
                            $nom_entreprise = $req_nom_entreprise->fetch(); 

                            $req_info_eleve = $bdd->query("SELECT * FROM eleves WHERE id_eleve ='". $donnees['id_eleve'] ."'");
                            $info_eleves = $req_info_eleve->fetch(); 

                            echo("<tr>
                            <th>"
                                .$nom_entreprise['Nom_entreprise'].
                            "</th>
                            <th>"
                                .$info_eleves['nom_eleve'].
                            "</th>
                            <th>"
                                .$info_eleves['prenom_eleve'].
                            "</th>
                            <th>"
                                .$donnees['Date_debut'].
                            "</th>
                            <th>"
                                .$donnees['Date_fin'].
                            "</th>
                            <th>
                                <a href= 'ajout_stage_post.php?Del=true&stage=". $donnees['id_stage'] ."' class='btn btn-secondary btn-sm active'> Supprimer </a>
                            </th>
                            </tr>
                            </tbody>
                            ");
                        }
                        echo("</table>");
                    }
                ?>


            </div>
        </div>


    </body>
</html>
