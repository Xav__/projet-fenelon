<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">

    <title>Fenelon - Recherche</title>

    <script>
        
        function show()
        {
            var titl = document.getElementById("titl");
            titl.style.visibility = "hidden";
        };
        
    </script>
</head>

<body>
    <?php
        include("navbar.php");
        
        $bdd = new PDO('mysql:host=localhost;dbname=projet_fenelon', 'root', '');
    ?>
    
    <!-- Section recherche -->
    <div class="container">
        <div class="row">

            <!-- Section recherche par élève -->
            <div class="col-sm">
                         
                <form method="post" action="Recherche_post.php?Ac=eleve">
                <p> Recherche par élève : </p>
                    <select name="id_eleve">
                        <?php

                            $reponse = $bdd->query('SELECT * FROM eleves ORDER BY nom_eleve');
                            
                            while ($donnees = $reponse->fetch())
                            {
                                ?>     
                                    <option value="<?php echo $donnees['id_eleve']; ?>"> <?php echo $donnees['nom_eleve']." ".$donnees['prenom_eleve']; ?></option>

                                <?php
                            }       

                            ?>
                    </select>

                    <input type="submit" value='Chercher'>

                </form>
            </div>  

            <!-- Section recherche par entreprise -->
            <div class="col-sm">
                <form method="post"  action="Recherche_post.php?Ac=En">
                <p> Recherche par entreprise : </p>
                    <select name="id_entreprise">
                        <?php

                            $reponse = $bdd->query('SELECT * FROM entreprises ORDER BY Nom_entreprise');
                            
                            while ($donnees = $reponse->fetch())
                            {
                                ?>     
                                    <option value="<?php echo $donnees['id_entreprise']; ?>"> <?php echo $donnees['Nom_entreprise']; ?></option>

                                <?php
                            }       

                            ?>
                    </select>

                    <input type="submit" value='Chercher'>

                </form>
            </div>  

            <!-- Section recherche par ville-->
            <div class="col-sm">
                <form method="post"  action="Recherche_post.php?Ac=Ville">

                    <p> Recherche par ville : </p>
                    <select name="Ville">
                        <?php

                            $reponse = $bdd->query('SELECT * FROM entreprises GROUP BY Ville ORDER BY Ville');

                            while ($donnees = $reponse->fetch()){
                                while ($donnees = $reponse->fetch())
                                {
                                    ?>     
                                        <option value="<?php echo $donnees['Ville']; ?>"> <?php echo $donnees['Ville']; ?></option>

                                    <?php
                                }   
                            }    
                                    ?>
                            
                    </select>

                    <input type="submit" value='Chercher'>

                </form>
            </div>  

        </div>  
    </div>  

    <br><br><br>

    <!-- Section d'affichages des informations selon la saisie de l'utilisateur -->
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                        
                        <?php

                            // tableau affichage avec une recherche par élève
                            if(isset($_GET['I'])){
                                $req_eleve = $bdd->query("SELECT * FROM stages WHERE id_eleve ='". $_GET['I'] ."'");

                                $req_nom_prenom_eleves = $bdd->query("SELECT * FROM eleves WHERE id_eleve ='". $_GET['I'] ."'");
                                $nom_prenom_eleves = $req_nom_prenom_eleves->fetch();

                                echo ("Recherche pour l'élève : " . $nom_prenom_eleves['nom_eleve'] . " ". $nom_prenom_eleves['prenom_eleve'] . "<br>");
                                
                                echo("<table class='table table-bordered'>
                                        <thead class='thead-light'>
                                            <tr>
                                                <th>
                                                    Nom de l'entreprise
                                                </th>
                                                <th>
                                                    Classe de l'élève
                                                </th>
                                                <th>
                                                    Date début stage
                                                </th>
                                                <th>
                                                    Date fin stage
                                                </th>
                                            </tr>
                                        </thead>
                                    <tbody>");
                                while ($donnees = $req_eleve->fetch()){

                                    $req_nom_entreprise = $bdd->query("SELECT Nom_entreprise FROM entreprises WHERE id_entreprise ='". $donnees['id_entreprise'] ."'");
                                    $nom_entreprise = $req_nom_entreprise->fetch();  

                                    echo("<tr>
                                    <th>"
                                        .$nom_entreprise['Nom_entreprise'].
                                    "</th>
                                    <th>"
                                        .$donnees['Classe_pendant_stage'].
                                    "</th>
                                    <th>"
                                        .$donnees['Date_debut'].
                                    "</th>
                                    <th>"
                                        .$donnees['Date_fin'].
                                    "</th>
                                    </tr>
                                    </tbody>
                                    ");
                                }
                                echo("</table>");
                            }

                            // tableau affichage avec une recherche par entreprise
                            if(isset($_GET['E'])){

                                $req_entreprise = $bdd->query("SELECT * FROM entreprises WHERE id_entreprise ='". $_GET['E']."'");
                                $info_entreprise = $req_entreprise->fetch();  
                                
                                echo("<table class='table table-bordered'>
                                        <thead class='thead-light'>
                                            <tr>
                                                <th>
                                                    Nom de l'entreprise
                                                </th>
                                                <th>
                                                    Adresse
                                                </th>
                                                <th>
                                                    Complément
                                                </th>
                                                <th>
                                                    Ville
                                                </th>
                                                <th>
                                                    Téléphone de l'entreprise
                                                </th>
                                                <th>
                                                    Mail de l'entreprise
                                                </th>
                                                <th>
                                                    Nom du Dirigeant
                                                </th>
                                                <th>
                                                    Mail du Dirigeant
                                                </th>
                                            </tr>
                                        </thead>
                                    <tbody>
                                        <tr>
                                            <th>"
                                                .$info_entreprise['Nom_entreprise'].
                                            "</th>
                                            <th>"
                                                .$info_entreprise['Adresse1'].
                                            "</th>");

                                        if($info_entreprise['Adresse2'] == $info_entreprise['Adresse3']){
                                            echo("<th>"
                                                .$info_entreprise['Adresse1'].
                                            "</th>");
                                            
                                        }else{
                                            echo("<th>"
                                                .$info_entreprise['Adresse1']. " - " .$info_entreprise['Adresse2'].
                                            "</th>");
                                        }
                                        echo("<th>"
                                                .$info_entreprise['Ville'].
                                            "</th>
                                            <th>"
                                                .$info_entreprise['Tel_entreprise'].
                                            "</th>
                                            <th>"
                                                .$info_entreprise['Mail_entreprise'].
                                            "</th>
                                            <th>"
                                                .$info_entreprise['Nom_dirigeant'].
                                            "</th>
                                            <th>"
                                                .$info_entreprise['Mail_dirigeant'].
                                            "</th>
                                            </tr>
                                            </tbody>
                                            </table><br>");

                                            /*////////////////////////*/
                                            
                                            $req_stages = $bdd->query("SELECT * FROM stages, entreprises WHERE entreprises.id_entreprise = stages.id_entreprise AND stages.id_entreprise ='". $_GET['E']."'");

                                            echo("<table class='table table-bordered'>
                                                    <thead class='thead-light'>
                                                        <tr>
                                                            <th>
                                                                Nom de l'élève
                                                            </th>
                                                            <th>
                                                                Prénom de l'élève
                                                            </th>
                                                            <th>
                                                                Classe
                                                            </th>
                                                            <th>
                                                                Nom du tuteur
                                                            </th>
                                                            <th>
                                                                Fonction du tuteur
                                                            </th>
                                                            <th>
                                                                Mail Tuteur
                                                            </th>
                                                            <th>
                                                                Téléphone Tuteur
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                <tbody>");

                                    while ($donnees = $req_stages->fetch()){
                                        
                                        $req_info_eleve = $bdd->query("SELECT nom_eleve, prenom_eleve FROM eleves, stages WHERE stages.id_eleve = eleves.id_eleve AND stages.id_eleve ='". $donnees['id_eleve']."'");
                                        $info_eleve = $req_info_eleve->fetch();

                                        echo("<tr>
                                        <th>"
                                            .$info_eleve['nom_eleve']. 
                                        "</th>
                                        <th>"
                                            .$info_eleve['prenom_eleve']. 
                                        "</th>
                                        <th>"
                                            .$donnees['Classe_pendant_stage'].
                                        "</th>
                                        <th>"
                                            .$donnees['Nom_tuteur'].
                                        "</th>
                                        <th>"
                                            .$donnees['Fonction_tuteur'].
                                        "</th>
                                        <th>"
                                            .$donnees['Mail_tuteur'].
                                        "</th>
                                        <th>"
                                            .$donnees['Tel_tuteur']. "<br>" .$donnees['Portable_tuteur'].
                                        "</th>
                                        </tr>
                                        </tbody>
                                        ");
                                    }
                                    echo("</table>");
                            }

                            // tableau affichage avec une recherche par Ville
                            if(isset($_GET['V'])){
                                $Ville = str_replace("%20", " ", $_GET['V']);

                                $sql_ville = $bdd->query("SELECT * FROM entreprises WHERE Ville ='". $Ville ."' ORDER BY Nom_entreprise");

                                echo ("Recherche par ville : " . $Ville . "<br>");


                                echo("<table class='table table-bordered'>
                                         <thead class='thead-light'>
                                            <tr>
                                                <th>
                                                    Nom de l'entreprise
                                                </th>
                                                <th>
                                                    Domaine
                                                </th>
                                                <th width='13%'>
                                                    Téléphone
                                                </th>
                                                <th>
                                                    Mail
                                                </th>
                                                <th>
                                                    Siret
                                                </th>
                                            </tr>
                                        </thead>
                                    <tbody>");

                                while ($donnees = $sql_ville->fetch()){
                                    echo("<tr>
                                    <th>"
                                        .$donnees['Nom_entreprise'].
                                    "</th>
                                    <th>"
                                        .$donnees['Domaine'].
                                    "</th>
                                    <th>"
                                        .$donnees['Tel_entreprise'].
                                    "</th>
                                    <th>"
                                        .$donnees['Mail_entreprise'].
                                    "</th>
                                    <th>"
                                        .$donnees['Siret'].
                                    "</th>
                                    </tr>
                                    </tbody>
                                    ");
                                }
                                echo("</table>");
                            } 
                        ?>

            </div>  
        </div>  
    </div> 

</body>
</html>