<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fenelon - Gestion Élève</title>
    <link href="style.css" rel="stylesheet">
</head>

<?php
        include("navbar.php");
        $bdd = new PDO('mysql:host=localhost;dbname=projet_fenelon', 'root', ''); 
?>

<body>

    <!-- Formulaire ajout d'eleve dans la bdd -->
    <div class="container">

        <form method="post" action="eleves_post.php">
        <label><b>Formulaire d'ajout d'un nouvel élève dans la base de données.</b></label>
        <br>
        <b> *  Les champs sont obligatoires. </b>

            <h1>Formulaire - Ajout Élève</h1>
            <label><b>Nom élève *</b></label>
            <input type="text" placeholder="Entrer le nom de l'élève" name="nom" required>

            <label><b>Prénom élève *</b></label>
            <input type="text" placeholder="Entrer le prénom de l'élève" name="prenom" required>
            
            <input type="submit" id='submit' value='Enregistrer' >

        </form>
    </div>

    <br><br><br>

    <!-- Différents affichages des infos messages selon les entrées de l'utilisateur -->
    <div class="container">
    
        <div class="col align-self-center">
            
            <?php

                if(isset($_GET['Va']) && $_GET['Va'] == 'true'){

                    echo("<h2 id='Info_message'> L'élève a bien été ajouté. </h2>");

                    $last_insert = $bdd->query('SELECT * FROM eleves ORDER BY id_eleve DESC LIMIT 1');
                    $donnees = $last_insert->fetch();               
                    echo ("<h4 id='Info_message'> Nom : ".$donnees['nom_eleve'] . "<br> Prénom : " . $donnees['prenom_eleve'] . "</h4>");

                }

                if(isset($_GET['Va']) && $_GET['Va'] == 'false'){

                    echo("<h2 id='Info_message'> L'élève n'a pas été ajouté. </h2>");

                    // un élève porte déjà le même nom et/ou prenom

                }

                if(isset($_GET['Del']) && $_GET['Del'] == 'true'){

                    echo("<h2 id='Info_message'> L'élève a bien été supprimé. </h2>");

                }

            ?>

        </div>

    </div>

    <br><br>

    <!-- Section suppresion d'élèves -->
    <div class="container">
        <div class="col align-self-center">
            <form method="post" action="eleves_post.php?Del=1">
            <label><b>Formulaire de suppression d'un élève dans la base de données.</b></label>
            <br>
                <select name ='id_eleve'>

                    <?php

                            $reponse = $bdd->query('SELECT * FROM eleves ORDER BY id_eleve DESC');

                            while ($donnees = $reponse->fetch()){
                            
                                echo("<option value='".  $donnees['id_eleve']."'> ". $donnees['nom_eleve']. " " .$donnees['prenom_eleve'] . "</option>");

                            }

                    ?>

                    <input type="submit" id='submit' value='Supprimer' >

                </select>
            </form>
        </div>
    </div>

</body>
</html>