<?php 
    session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title id="titre">Fenelon - Accueil</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

    <body>

        
        <br><br>


        <div class="container"> 
            <!-- zone de connexion -->
            
            <img src="images/logo-Fenelon-Notre-Dame-ensemble-scolaire-La-Rochelle.jpg" id="imgFenelon"/>

            <form action="verification.php" method="POST">
            <label><b>Formulaire de connexion à la base de données.</b></label>
            <br>
            <h1>Fenelon - Connexion</h1>
            <label><b>Nom d'utilisateur</b></label>
            <input type="text" placeholder="Entrer le nom d'utilisateur" name="nom" required>

                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="mdp" required>

                <input type="submit" id='submit' value='LOGIN' >
                <a class="btn btn-secondary" href="inscription.php">S'inscrire</a>
            </form>
        </div>
    </body>
</html>
