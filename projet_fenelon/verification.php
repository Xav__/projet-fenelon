<?php
session_start();

if(isset($_POST['nom']) && isset($_POST['mdp']))
{
    // connexion à la base de données

    $mysqli = mysqli_connect("localhost", "root", "", "projet_fenelon");
    if (mysqli_connect_errno()) {
        echo "Echec lors de la connexion à MySQL : " . mysqli_connect_error();
    }

    // on applique les deux fonctions mysqli_real_escape_string et htmlspecialchars
    // pour éliminer toute attaque de type injection SQL et XSS
    $nom = mysqli_real_escape_string($mysqli,htmlspecialchars($_POST['nom'])); 
    $mdp = mysqli_real_escape_string($mysqli,htmlspecialchars($_POST['mdp']));
    
    if($nom != "" && $mdp != "")
    {
        $requete = "SELECT count(*) FROM utilisateurs WHERE mdp = '".md5($mdp)."' ";
        $exec_requete = mysqli_query($mysqli,$requete);
        $reponse      = mysqli_fetch_array($exec_requete);
        $count = $reponse['count(*)'];
        if($count!=0) // nom d'utilisateur et mot de passe correctes
        {
           $_SESSION['nom'] = $nom;
           header('Location: principale.php');
        }
        else
        {
         header('Location: connexion.html?erreur=1'); // utilisateur ou mot de passe incorrect
        }
    }
  }

  else
  {
    header('Location: connexion.html?erreur=2'); // utilisateur ou mot de passe vide
  }

mysqli_close($db); // fermer la connexion
?>