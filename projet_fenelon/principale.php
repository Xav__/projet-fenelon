<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet">

    <title>Fenelon - Accueil</title>
</head>

    <?php
            include("navbar.php");
    ?>

<body>
    <div class="container">   
    <br>
    <br>
    <div class="ensemble_logo">
        <div class="row">

            <div class="col-sm">

                <h2> Gestion Eleve </h2>
                <a href="Eleves.php"><img src="images/stagiaire.png" alt="Eleves" class="img-fluid"/></a>

            </div>

            <div class="col-sm">

                <h2> Gestion Entreprise </h2>
                <a href="Entreprises.php"><img src="images/entreprise.jpg" alt="Entreprises" class="img-fluid" /></a>

            </div>
            
                
            <div class="col-sm">

                <h2> Gestion Stage </h2>
                <a href="AjoutStage.php"><img src="images/stage.jpg" alt="formulaire" class="img-fluid" /></a>

            </div>

            <div class="col-sm">
                <h2> Recherche </h2>
                <a href="RechercheStage.php"><img src="images/chercher.png" alt="Recherche" class="img-fluid"/></a>

            </div>
            
            <div class="col-sm">

                <h2> Aide </h2>
                <a href="explication.php"><img src="images/aide.jpg" alt="Recherche" class="img-fluid"/></a>

            </div>
        </div>        
    </div>
    <div class="logofnd">

    <img src="images/logo-Fenelon-Notre-Dame-ensemble-scolaire-La-Rochelle1.jpg" alt="Recherche">

</div>

</body>
</html>

