<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fenelon - Gestion Entreprise</title>
    <link href="style.css" rel="stylesheet">
</head>

    <?php
            include("navbar.php");
            $bdd = new PDO('mysql:host=localhost;dbname=projet_fenelon', 'root', '');
    ?>

<body>

    <!-- Formulaire ajout entreprise dans la bdd -->
    <div class="container">

        <form method="post" action="entreprises_post.php">
            <label><b>Formulaire d'ajout d'une nouvelle entreprise dans la base de données.</b></label>
            <br>
            <b> *  Les champs sont obligatoires. </b>

            <h1>Formulaire - Ajout Entreprise</h1>
            <label><b>Nom Entreprise *</b></label>
            <input type="text" placeholder="Entrer le nom de l'entreprise" name="nom_Entreprise" required>

            <label><b>Domaine *</b></label>
            <input type="text" placeholder="Entrer le domaine de l'entreprise" name="Domaine" required>

            <label><b>Siret * (14 chiffres sont attendues)</b>
            <?php
                if(isset($_GET['Siret']) && $_GET['Siret'] == 'false'){
                    echo("<h4 id='Info_message'> Le numéro de SIRET entré ne faisait pas 14 caractères. </h4>");
                }
            ?>
            </label>
            <input type="text" placeholder="Entrer le numéro de Siret" name="Siret" required>

            <label><b>NAF (4 chiffres & 1 lettre attendues)</b>
            <?php
                if(isset($_GET['NAF']) && $_GET['NAF'] == 'false'){
                    echo("<h4 id='Info_message'> Le NAF doit être composé de 4 chiffres et une lettre. </h4>");
                }
            ?>
            </label>
            </label>
            <input type="text" placeholder="Entrer le numéro de NAF" name="NAF">

            <label><b>Adresse 1 *</b></label>
            <input type="text" placeholder="Entrer l'adresse 1" name="adresse1" required>

            <label><b>Adresse 2</b></label>
            <input type="text" placeholder="Entrer l'adresse 2" name="adresse2">

            <label><b>Adresse 3</b></label>
            <input type="text" placeholder="Entrer l'adresse 3" name="adresse3" >

            <label><b>Code Postale *</b></label>
            <input type="text" placeholder="Entrer le code postale" name="CP" required>

            <label><b>Ville *</b></label>
            <input type="text" placeholder="Entrer la ville" name="Ville" required>

            <label><b>Telephone entreprise</b></label>
            <input type="text" placeholder="Entrer le téléphone de l'entreprise" name="tel_entreprise"> 
            <br>
            <label for="exampleInputEmail1"> Mail de l'entreprise *</label>
            <input type="email" class="form-control" name="mail_entreprise" aria-describedby="emailHelp" placeholder="Entrer le mail de l'entreprise" required>
            <br>
            <label><b>Nom dirigeant *</b></label>
            <input type="text" placeholder="Entrer le nom du dirigeant" name="nom_dirigeant" required>

            <label><b>Prénom dirigeant</b></label>
            <input type="text" placeholder="Entrer le prénom du dirigeant" name="prenom_dirigeant" >

            <label><b>Fonction dirigeant</b></label>
            <input type="text" placeholder="Entrer la fonction du dirigeant" name="fonction_dirigeant" >
            <br>
            <label for="exampleInputEmail1"> Mail dirigeant</label>
            <input type="email" class="form-control" name="mail_dirigeant" aria-describedby="emailHelp" placeholder="Entrer le mail du dirigeant">
            <br>
            <input type="submit" id='submit' value='Enregistrer' >

        </form>
    </div>

    <br><br><br>


    <!-- Différents affichages des infos messages selon les entrées de l'utilisateur -->
    <div class="container">

        <div class="col align-self-center">
                
            <?php

                if(isset($_GET['Va']) && $_GET['Va'] == 'true'){

                    echo("<h2 id='Info_message'> L'entreprise a bien été ajoutée. </h2>");
                    $last_insert = $bdd->query('SELECT * FROM entreprises ORDER BY id_entreprise DESC LIMIT 1');
                    $donnees = $last_insert->fetch();               
                    echo ("<h4 id='Info_message'> Nom : ".$donnees['Nom_entreprise'] . "<br> Domaine : " . $donnees['Domaine'] . "<br> Siret : " .$donnees['Siret'] . "<br> NAF : " .$donnees['NAF'] . "<br> Adresse : " .$donnees['Adresse1'] . "<br> CP : " .$donnees['CP'] . "<br> Ville : " .$donnees['Ville'] . "<br> Mail entreprise : " .$donnees['Mail_entreprise'] . "<br> Nom dirigeant : " .$donnees['Nom_dirigeant'] . "<br> Mail dirigeant : " .$donnees['Mail_dirigeant'] . "</h4>");

                }

                if(isset($_GET['Va']) && $_GET['Va'] == 'false'){

                    echo("<h2 id='Info_message'> L'entreprise n'a pas été ajouté. </h2>");

                }

                if(isset($_GET['Del']) && $_GET['Del'] == 'true'){

                    echo("<h2 id='Info_message'> L'entreprise a bien été supprimé. </h2>");

                }

            ?>
        </div>
    </div>


    <!-- Section suppression des entreprises -->
    <div class="container">
        <div class="col align-self-center">
            <form method="post" action="entreprises_post.php?Del=1">
                <label><b>Formulaire de suppression d'une entreprise dans la base de données.</b></label>
                <br>
                <select name ='id_entreprise'>

                    <?php

                            $reponse = $bdd->query('SELECT * FROM entreprises ORDER BY id_entreprise DESC');

                            while ($donnees = $reponse->fetch()){
                            
                                echo("<option value='".  $donnees['id_entreprise']."'> ". $donnees['Nom_entreprise']. " - " .$donnees['Ville'] . "</option>");

                            }

                    ?>

                    <input type="submit" id='submit' value='Supprimer' >

                </select>
            </form>
        </div>
    </div>

</body>
</html>