<?php

    //On établit la connexion
    $mysqli = mysqli_connect("localhost", "root", "", "projet_fenelon");
    if (mysqli_connect_errno()) {
        echo "Echec lors de la connexion à MySQL : " . mysqli_connect_error();
    }

     /* on test si les champ sont bien remplis */
        if(!empty($_POST['classe_pdt_stage']) and !empty($_POST['date_debut']) and !empty($_POST['date_fin']) and !empty($_POST['nom_tuteur']) and !empty($_POST['fonction_tuteur']) and !empty($_POST['mail_tuteur']) and !empty($_POST['nom_prof']))
        {
            $id_eleve = $_POST['id_eleve'];
            $id_entreprise = $_POST['id_entreprise'];
            $classe_stage = str_replace("'", " ", $_POST['classe_pdt_stage']);
            $date_debut = str_replace("'", " ", $_POST['date_debut']);
            $date_fin = str_replace("'", " ", $_POST['date_fin']);
            $nom_tuteur = str_replace("'", " ", $_POST['nom_tuteur']);
            $prenom_tuteur = str_replace("'", " ", $_POST['prenom_tuteur']);
            $fonction_tuteur = str_replace("'", " ", $_POST['fonction_tuteur']);
            $tel_tuteur = str_replace("'", " ", $_POST['tel_tuteur']);
            $portable_tuteur = str_replace("'", " ", $_POST['portable_tuteur']);
            $mail_tuteur = str_replace("'", " ", $_POST['mail_tuteur']);
            $nom_prof = str_replace("'", " ", $_POST['nom_prof']);
            $prenom_prof = str_replace("'", " ", $_POST['prenom_prof']);

            if($date_debut < $date_fin){

                //On créé la requête
                $sql_ajoutstage = "INSERT INTO `stages`(id_entreprise, id_eleve, Classe_pendant_stage, Date_debut, Date_fin, Nom_tuteur, Prenom_tuteur, Fonction_tuteur, Tel_tuteur, Portable_tuteur, Mail_tuteur, Nom_prof, Prenom_prof) VALUES ('$id_entreprise', '$id_eleve', '$classe_stage', '$date_debut', '$date_fin', '$nom_tuteur', '$prenom_tuteur', '$fonction_tuteur', '$tel_tuteur', '$portable_tuteur', '$mail_tuteur',' $nom_prof', '$prenom_prof')";         

                $prepareUtilisateur = $mysqli->prepare($sql_ajoutstage);

                $prepareUtilisateur->execute();

                header('Location: AjoutStage.php?Va=true');  
            }
            else{
                header('Location: AjoutStage.php?Va=false&Date=false');
            }

        }
        

        //permet de renvoyer l'id de l'élève dans l'url pour afficher le tableau pour la suppression de ces stages 
        elseif(isset($_GET['etape']) && $_GET['etape'] == 1) {

            $id_eleve = $_POST['id_eleve'];
            $location = "AjoutStage.php?E=";
            $location .= $id_eleve;
            header("Location: ".$location);  
            exit;
        }

        // suprime un stage de la bdd
        elseif($_GET['Del'] !='' && $_GET['Del'] =='true')
        {

            $id_stage = $_GET['stage']; 

            $sql_del_stage = "DELETE FROM stages WHERE id_stage = $id_stage";
            $req_deleteProjet = $mysqli->prepare($sql_del_stage);
            $req_deleteProjet ->execute(); 

            header('Location: AjoutStage.php?Del=true');  
        
        }

        else {
            
            header('Location: AjoutStage.php?Va=false');  
        }

?>