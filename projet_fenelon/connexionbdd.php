<?php
// Informations d'identification
$mysqli = mysqli_connect("localhost", "root", "", "projet_fenelon");
if (mysqli_connect_errno()) {
    echo "Echec lors de la connexion à MySQL : " . mysqli_connect_error();
}
// Vérifier la connexion
if($mysqli === false){
    die("ERREUR : Impossible de se connecter. " . mysqli_connect_error());
}
?>