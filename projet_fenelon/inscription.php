<?php 

    session_start();
?>
<!DOCTYPE html>
<head>
    
    <title>Fenelon - Inscription</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="style.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <meta charset="utf-8">
    
</head>


     
    <div class="container">
    <img src="images/logo-Fenelon-Notre-Dame-ensemble-scolaire-La-Rochelle.jpg" id="imgFenelon"/>

        <form method="post" action="">
        <label><b>Formulaire d'inscription à la base de données.</b></label>
            <br>
            <h1>Fenelon - Inscription</h1>

            <label><b>Nom</b></label>
            <input type="text" name="Nom">
            <label><b>Prenom</b></label>
            <input type="text" name="Prenom">
            <label><b>Mot de passe</b></label>
            <input type="password" name="mdp">
            <label><b>Répetez votre mot de passe</b></label>
            <input type="password" name="repeatpassword"><br><br>
            <input type="submit" name="submit" value="Valider">
        
        </form>
    </div>
<?php
//On établit la connexion
$mysqli = mysqli_connect("localhost", "root", "", "projet_fenelon");
if (mysqli_connect_errno()) {
    echo "Echec lors de la connexion à MySQL : " . mysqli_connect_error();
}
      
if (isset($_POST['submit']))
{
   /* on test si les champ sont bien remplis */
    if(!empty($_POST['Nom']) and !empty($_POST['Prenom'])and !empty($_POST['mdp']) and !empty($_POST['repeatpassword']))
    {   
        /* on test si le mdp contient bien au moins 6 caractère */
        if (strlen($_POST['mdp'])>=3)
        {
            /* on test si les deux mdp sont bien identique */
            if (isset($_POST['mdp']) && $_POST['mdp']==$_POST['repeatpassword'])
            {
                // On crypte le mot de passe
                $_POST['mdp']= md5($_POST['mdp']);
                // on se connecte à MySQL et on sélectionne la base
                $bdd = new PDO('mysql:host=localhost;dbname=projet_fenelon', 'root', ''); 
                //On créé la requête

                $Nom = str_replace("'", " ", $_POST['Nom']);
                $Prenom = str_replace("'", " ", $_POST['Prenom']);
                $mdp = str_replace("'", " ", $_POST['mdp']);



                $sql = "INSERT INTO utilisateurs(nom, prenom, mdp) VALUES ('$Nom','$Prenom','$mdp')";
                header('Location: connexion.php');
                /* execute et affiche l'erreur mysql si elle se produit */
                $inscription = $mysqli->prepare($sql);

                $inscription->execute();
            }
            else echo "Les mots de passe ne sont pas identiques";
        }
        else echo "Le mot de passe est trop court !";
    }
    else echo "Veuillez saisir tous les champs !";
}
?>
</body>
</html>